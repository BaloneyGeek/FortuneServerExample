#-------------------------------------------------
#
# Project created by QtCreator 2014-11-27T20:22:57
#
#-------------------------------------------------

QT       += core
QT       += network
QT       -= gui

TARGET = FortuneServer
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    fortuneserver.cpp

HEADERS += \
    fortuneserver.h
